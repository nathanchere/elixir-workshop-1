# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :absence_register,
  ecto_repos: [AbsenceRegister.Repo]

# Configures the endpoint
config :absence_register, AbsenceRegisterWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "ob41oGpIckPTjmuwaFgQxxZccm/pg6QzXYpUizwrDA5kcFDUp4BZbtIP+DU7vks/",
  render_errors: [view: AbsenceRegisterWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: AbsenceRegister.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

config :ueberauth, Ueberauth,
  providers: [
    google: {Ueberauth.Strategy.Google, [default_scope: "profile email openid"]}
  ]

config :ueberauth, Ueberauth.Strategy.Google.OAuth,
  client_id: "906308106774-2s6nr6arhf38odlup2vc5rnmpjb7840k.apps.googleusercontent.com",
  client_secret: "10BenZMg2eIZdROHDAX96_Mr"

config :absence_register, :valid_domain, "@tretton37.com"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
