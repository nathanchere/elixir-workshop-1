defmodule AbsenceRegister.Repo.Migrations.CreateAbsenceRequest do
  use Ecto.Migration

  def change do
    create table(:absence_request) do
      add :start_date, :date
      add :end_date, :date
      add :request_reason, :string
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:absence_request, [:user_id])
  end
end
