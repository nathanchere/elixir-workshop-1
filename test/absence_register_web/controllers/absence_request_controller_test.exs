defmodule AbsenceRegisterWeb.AbsenceRequestControllerTest do
  use AbsenceRegisterWeb.ConnCase

  alias AbsenceRegister.AbsenceRequests

  @create_attrs %{end_date: ~D[2010-04-17], request_reason: "some request_reason", start_date: ~D[2010-04-17]}
  @update_attrs %{end_date: ~D[2011-05-18], request_reason: "some updated request_reason", start_date: ~D[2011-05-18]}
  @invalid_attrs %{end_date: nil, request_reason: nil, start_date: nil}

  def fixture(:absence_request) do
    {:ok, absence_request} = AbsenceRequests.create_absence_request(@create_attrs)
    absence_request
  end

  describe "index" do
    test "lists all absence_request", %{conn: conn} do
      conn = get conn, absence_request_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Absence request"
    end
  end

  describe "new absence_request" do
    test "renders form", %{conn: conn} do
      conn = get conn, absence_request_path(conn, :new)
      assert html_response(conn, 200) =~ "New Absence request"
    end
  end

  describe "create absence_request" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, absence_request_path(conn, :create), absence_request: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == absence_request_path(conn, :show, id)

      conn = get conn, absence_request_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Absence request"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, absence_request_path(conn, :create), absence_request: @invalid_attrs
      assert html_response(conn, 200) =~ "New Absence request"
    end
  end

  describe "edit absence_request" do
    setup [:create_absence_request]

    test "renders form for editing chosen absence_request", %{conn: conn, absence_request: absence_request} do
      conn = get conn, absence_request_path(conn, :edit, absence_request)
      assert html_response(conn, 200) =~ "Edit Absence request"
    end
  end

  describe "update absence_request" do
    setup [:create_absence_request]

    test "redirects when data is valid", %{conn: conn, absence_request: absence_request} do
      conn = put conn, absence_request_path(conn, :update, absence_request), absence_request: @update_attrs
      assert redirected_to(conn) == absence_request_path(conn, :show, absence_request)

      conn = get conn, absence_request_path(conn, :show, absence_request)
      assert html_response(conn, 200) =~ "some updated request_reason"
    end

    test "renders errors when data is invalid", %{conn: conn, absence_request: absence_request} do
      conn = put conn, absence_request_path(conn, :update, absence_request), absence_request: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Absence request"
    end
  end

  describe "delete absence_request" do
    setup [:create_absence_request]

    test "deletes chosen absence_request", %{conn: conn, absence_request: absence_request} do
      conn = delete conn, absence_request_path(conn, :delete, absence_request)
      assert redirected_to(conn) == absence_request_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, absence_request_path(conn, :show, absence_request)
      end
    end
  end

  defp create_absence_request(_) do
    absence_request = fixture(:absence_request)
    {:ok, absence_request: absence_request}
  end
end
