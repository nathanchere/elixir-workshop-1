defmodule AbsenceRegisterWeb.NavTemplateTest do
  use AbsenceRegisterWeb.ConnCase

  import Phoenix.View, only: [render_to_string: 3]
  alias AbsenceRegisterWeb.LayoutView
  alias AbsenceRegister.User

  describe "When not signed in" do
    test "shows a 'Sign in' link", %{conn: conn} do
      result = render_to_string(LayoutView, "nav.html", conn: conn)

      assert(result =~ "Sign In")
    end
  end

  describe "When signed in" do
    test "shows a 'Sign out' link", %{conn: conn} do
      conn = conn |> assign(:user, %User{})

      result = render_to_string(LayoutView, "nav.html", conn: conn)

      assert(result =~ "Sign Out")
    end
  end
end
