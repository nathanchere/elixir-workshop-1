# Absence Register

A gentle hands-on introduction to Elixir. Learn by doing.

## Pre-requisites

- Elixir (... Erlang OTP)
- PostgreSQL
- npm

It is strongly recommended to develop on Linux. That does not mean "Windows 10 + WSL" and no support is offered for this environment. It will work on Windows but there a lot of annoying bullshit bugs and limitations you'll encounter along the way. They can be worked around but it's not ideal. OSX users should be able to mostly follow along the same as Linux but again no support is offered if you run into any unanticipated problems.

## Nice To Have

- Editor - main recommended options are Spacemacs (with Alchemist), VS Code or Atom.
- an application configured in [Google Developer Console](https://console.developers.google.com/home) if you want to use your own authentication keys
- PostgreSQL client
  - [DBeaver](https://dbeaver.io)

## Walkthrough

Throughout the walkthrough are milestone labels. You can skip to a particular point by checking out the corresponding tagged commit, e.g. `git checkout milestone-1`

### Basic setup

- Create a new application with `mix phx.new absence_request`
- Edit `config/dev.exs` with your PostgreSQL settings (and optionally change HTTP port from 4000 to 1337)
- If you intend on using version control, add the following to your .gitignore: `/.elixir_ls`

### Running for the first time (_milestone-1_)

- `mix deps.get` - equivalent to `dotnet restore`, `npm install` etc
- `cd assets` and `npm install` - build front-end/browser concerns (then `cd ..`)
- `mix ecto.create` - to ensure the backend database exists
- `mix ecto.migrate` -
- `mix phx.server` - compile the project and start the server

### Replacing the stock template (_milestone-2_)

- Add a `valid_domain` line to `config/config.exs` so it resembles the following:

  ```
  config :absence_register, :valid_domain, "@tretton37.com"
  ```

- `lib/absence_register_web/templates/layout/index.html.eex` - replace the entire content with:

  ```
    <div class="jumbotron">
      <p class="lead">Review and mange your absence requests with ease.<br />Proudly 100% Visma-free!</p>
    </div>

    <div class="text-center">
      <h4>Sign in with your <%= Application.get_env(:absence_register, :valid_domain) %> account to get started</h4>
    </div>
  ```

- `lib/absence_register_web/templates/layout/app.html.eex` - replace the nav bar / heading tag's contents with `<%= render "nav.html" %>`. The entire body should resemble something like this:
  ```
  <body>
    <div class="container">
      <header class="header">
        <%= render "nav.html" %>
      </header>
      <p class="alert alert-info" role="alert"><%= get_flash(@conn, :info) %></p>
      <p class="alert alert-danger" role="alert"><%= get_flash(@conn, :error) %></p>
      <main role="main">
        <%= render @view_module, @view_template, assigns %>
      </main>
    </div>
    <script src="<%= static_path(@conn, "/js/app.js") %>"></script>
  </body>
  ```
- create a new file `lib/absence_register_web/templates/layout/app.html.eex`. Add some code to replace the heading we just removed:

  ```
  <div class="clearfix">
      <nav>
          <ul class="nav pull-right">
              <li class="active"><a href="#">Sign In</a></li>
          </ul>
      </nav>
      <h3 class="text-muted">Absence Register</h3>
  </div>
  ```

### Adding third-party sign-in (_milestone-3_)

- Add Überauth to the project dependencies in `./mix.exs`:

  ```
    defp deps do
      [
        # ... other dependencies here
        {:ueberauth_google, "~> 0.7"}
      ]
    end
  ```

  Confirm it worked with `mix deps.get`

- Configure a new application in Google Developer Console with OAuth2 sign-on. Make sure you add `http://localhost:1337` as an authorised JavaScript origin and `http://localhost:1337/auth/google/callback` as an authoirised redirect URI. If you want to skip this step, here are some demo credentials:
  Client ID: `906308106774-2s6nr6arhf38odlup2vc5rnmpjb7840k.apps.googleusercontent.com`
  Client Secret: `10BenZMg2eIZdROHDAX96_Mr`

- Add the settings to `./config/config.exs`:

  ```
  config :ueberauth, Ueberauth,
    providers: [
      google: {Ueberauth.Strategy.Google, [default_scope: "profile email openid"]}
    ]

  config :ueberauth, Ueberauth.Strategy.Google.OAuth,
    client_id: "YOUR CLIENT ID",
    client_secret: "YOUR CLIENT SECRET"
  ```

- Create a new file `./absence_register_web/controllers/auth_controller.ex` with the following placeholder code:

  ```
  defmodule AbsenceRegisterWeb.AuthController do
    use AbsenceRegisterWeb, :controller
    plug(Ueberauth)

  end
  ```

- Add new routes to `./lib/absence_register_web/router.ex` to support the "Sign In" link and the result callback from Google:

  ```
  scope "/auth", AbsenceRegisterWeb do
    pipe_through(:browser)

    get("/google", AuthController, :request)
    get("/google/callback", AuthController, :callback)
  end
  ```

- Update the "Sign In" link in `./lib/absence_register_web/templats/layout/nav.html.eex` to point to `/login`

### Handling the authentication response and managing user sessions (_milestone-4_)

- Add the following function to `./lib/absence_register_web/controllers/auth_controller.ex` to handle the callback from Google for successful authentication:

  ```
  def callback(%{assigns: %{ueberauth_auth: auth}} = conn, _params) do
    user = %{
      token: auth.credentials.token,
      name: "#{auth.info.first_name} #{auth.info.last_name}",
      email: auth.info.email
    }

    conn
    |> put_flash(:info, "Successfully signed in as #{user.name}")
    |> put_session(:user_id, user.email)
    |> redirect(to: page_path(conn, :index))
  end
  ```

- Add the following to the same file to handle the callback from Google for failed authentication:

  ```
  def callback(%{assigns: %{ueberauth_failure: auth}} = conn, _params) do
    [error | _] = auth.errors

    conn
    |> put_flash(:error, error.message)
    |> redirect(to: page_path(conn, :index))
  end
  ```

- Add the following to the same file to handle requests to sign out:

  ```
  def signout(conn, _params) do
    conn
    |> configure_session(drop: true)
    |> redirect(to: page_path(conn, :index))
  end
  ```

### Displaying different info for authenticated users (_milestone-5_)

- Create a new file at `./lib/absence_register_web/plugs/set_user.ex` with the following:

  ```
  defmodule AbsenceRegisterWeb.Plugs.SetUser do
    import Plug.Conn

    def init(_params) do
    end

    def call(conn, _params) do
      if conn.assigns[:user] do
        conn
      else
        user_email = get_session(conn, :user_id)

        cond do
          user = user_email ->
            assign(conn, :user, user_email)

          true ->
            assign(conn, :user, nil)
        end
      end
    end
  end
  ```

- Add a reference to the new plug to the end of the `:browser` pipeline in `./lib/absence_register_web/router.ex`:

  ```
    plug(AbsenceRegisterWeb.Plugs.SetUser)
  ```

- Replace the contents of `./lib/absence_register_web/templates/layout/nav.html.eex` with:

  ```
  <div class="clearfix">
    <h3 class="text-muted pull-left">Absence Register</h3>
    <%= if @conn.assigns[:user] do
            render "nav_user.html", conn: @conn
        else
            render "nav_guest.html", conn: @conn
        end
    %>
  </div>
  ```

- Create a new file `./lib/absence_register_web/templates/layout/nav_guest.html.eex` to replace the guest sign-in content we just removed:

  ```
  <nav class="pull-right">
    <ul class="nav ">
        <li class="active"><a href="/auth/google">Sign In</a></li>
    </ul>
  </nav>
  ```

- Create a new file `./lib/absence_register_web/templates/layout/nav_user.html.eex` with a greeting for signed-in users and a sign-out link:

  ```
  <p class="text-right">
    Welcome <b><%= @conn.assigns.user %></b>
  </p>
  <nav class="pull-right">
    <ul class="nav ">
        <li class="active">
            <a href="/auth/signout">Sign Out</a>
        </li>
    </ul>
  </nav>
  ```

- Change the call to render the `nav.html` template in `./lib/absence_register_web/templates/layout/app.html.eex` so it passes the connection instance:

  ```
  <%= render "nav.html", conn: @conn %>
  ```

- Add a new route at the end of the `/auth` scope (just before the `end` statement) in `./lib/absence_register_web/router.ex` to handle sign-out requests:

  ```
  get("/signout", AuthController, :signout)
  ```

### Creating the local database and user accounts (_milestone-6_)

- Run `mix phx.gen.schema User users email:string name:string token:string` to create our local user definition

- A new migration file will be have been created in `./priv/repo/migrations/`_timestamp_`_create_users.exs`. Add `create(unique_index(:users, [:email]))` after the `create table` block, so the entire `change` function should look like this:

  ```
  def change do
    create table(:users) do
      add(:email, :string)
      add(:name, :string)
      add(:token, :string)

      timestamps()
    end

    create(unique_index(:users, [:email]))
  end
  ```

- Run `mix ecto.create` and then `mix ecto.migrate` to create our database and the new `Users` table

### Persisting user information and sessions (_milestone-7_)

- Create a new file `./lib/absence_register/users.ex` with the following content:

  ```
  defmodule AbsenceRegister.Users do
    require Ecto.Query

    alias AbsenceRegister.Repo
    alias AbsenceRegister.User

    def list_users do
      Repo.all(User)
    end

    def insert_or_update_user(%{email: email} = attrs) do
      changeset =
        case Repo.get_by(User, email: email) do
          nil ->
            User.changeset(%User{}, attrs)

          user ->
            User.changeset(user, Map.put(attrs, :id, user.id))
        end

      Repo.insert_or_update(changeset)
    end

    def get_user!(email), do: Repo.get_by!(User, email: email)
  end
  ```

- Add `alias AbsenceRegister.Users` to the top of `./lib/absence_register_web/controllers/auth_controller.ex` to the first few lines should look like this:

  ```
  defmodule AbsenceRegisterWeb.AuthController do
    use AbsenceRegisterWeb, :controller
    plug(Ueberauth)

    alias AbsenceRegister.Users
  ```

- In the "success" `callback` of the same file, replace the `conn` statement so the function instead looks like this:

  ```
  def callback(%{assigns: %{ueberauth_auth: auth}} = conn, _params) do
    user_attrs = %{
      token: auth.credentials.token,
      name: "#{auth.info.first_name} #{auth.info.last_name}",
      email: auth.info.email
    }

    signin(conn, user_attrs)
  end
  ```

- Still in `./lib/absence_register_web/controllers/auth_controller.ex`, the code we just replaced will go into a new `signin` function:

  ```
  def signin(conn, user_attrs) do
    case Users.insert_or_update_user(user_attrs) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "Successfully signed in as #{user.name}")
        |> put_session(:user_id, user.email)
        |> redirect(to: page_path(conn, :index))

      {:error, _} ->
        conn
        |> clear_session()
        |> put_flash(:error, "Error signing in")
        |> redirect(to: page_path(conn, :index))
    end
  end
  ```

- In `./lib/absence_register_web/plugs/set_user.ex` add `alias AbsenceRegister.Users` to the top of the file and then change the first branch in the `cond` statement of `call` to:

  ```
  user = user_email && Users.get_user(user_email) ->
    assign(conn, :user, user)
  ```

- In `./lib/absence_register_web/templates/layout/nav_user.html.eex` change the welcome text to `Welcome <b><%= @conn.assigns.user.name %></b>`

### Registering absence requests (_milestone-8_)

- From your terminal run:

  ```
  mix phx.gen.html AbsenceRequests AbsenceRequest absence_request user_id:references:users start_date:date end_date:date request_reason:string
  ```

  Remember to run `mix ecto.migrate` afterwards.

- In `./lib/absence_register_web/router.ex` add a new `:require_auth` pipeline definition directly below the existing `:browser` pipeline:

  ```
  pipeline :require_auth do
    plug(:browser)
  end
  ```

- Still in `./lib/absence_register_web/router.ex`, add a new `scope` block to register our absence request routes:

  ```
  scope "/", AbsenceRegisterWeb do
    pipe_through(:require_auth)

    resources("/absences", AbsenceRequestController)
  end
  ```

### Automated tests and test-driven-ish development (_milestone-9_)

- Replace the one failing test in `./test/absence_register_web/controllers/page_controller_test.exs` with:

  ```
  test "GET / as guest", %{conn: conn} do
    validDomain = Application.get_env(:absence_register, :valid_domain)
    expected = "Sign in with your #{validDomain} account to get started"

    conn = get(conn, "/")
    assert html_response(conn, 200) =~ expected
  end
  ```

- Add another test in the same file for what authenticated users should see:

  ```
  test "GET / as authenticated user", %{conn: conn} do
    not_expected = "Sign in with your"

    conn =
      conn
      |> assign(:user, %AbsenceRegister.User{})
      |> get("/")

    assert not (html_response(conn, 200) =~ not_expected)
  end
  ```

- To make this test pass, wrap the sign-in prompt in `./lib/absence_register_web/templates/page/index.html.eex` in conditional tags:

  ```
  <%= if (@conn.assigns[:user]) do %>
  <% else %>
    <div class="text-center">
      <h4>Sign in with your <%= Application.get_env(:absence_register, :valid_domain) %> account to get started</h4>
    </div>
  <% end %>
  ```

- Create a new test file `./test/absence_register_web/templates/nav_test.exs` with the following:

  ```

  ```

### Limiting visibility of other users' requests (_milestone-10_)

## Miscellaneous tips

- Run an interactive Elixir (iex) session with `iex -S mix phx.server`
- Type `recompile` in iex for live reload in many instances
- Use `mix phx.routes` if you need to debug or review your defined routes
- Use `mix ecto.drop` `mix ecto.create` `mix ecto.migrate` if you want to wipe your database
- Use `IO.inspect(somevalue)` to write the content of `somevalue` to your terminal. Very useful for debugging. `IO.inspect/1` returns the inspected value, so you can use it between pipes too.q1
- To use Visual Studio Code debugging, add the ElixirLS extension and add this to your project's `./.vscode/launch.json`:

```
{
"type": "mix_task",
"name": "mix phx.server",
"request": "launch",
"projectDir": "${workspaceRoot}",
"task": "phx.server"
}
```

You can now 'debug' your application with `F5`. Some features like breakpoints work pretty well. Others like expression evaluation and watches... not so crash hot.

## Examples

- Überauth success response (Google OAuth2)

```
%Ueberauth.Auth{
credentials: %Ueberauth.Auth.Credentials{
expires: true,
expires_at: 1534351504,
other: %{},
refresh_token: nil,
scopes: ["https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/userinfo.email"],
secret: nil,
token: "tr1077.Glz6BVHe45vvZZSM_VQ",
token_type: "Bearer"
},
extra: "you can ignore this",
info: %Ueberauth.Auth.Info{
description: nil,
email: "bobby.tables@example.com",
first_name: "Bobby",
image: "https://some.cdn.com/photo.jpg",
last_name: "Tables",
location: nil,
name: "Bobby Tables",
nickname: nil,
phone: nil,
urls: %{profile: nil, website: "example.com"}
},
provider: :google,
strategy: Ueberauth.Strategy.Google,
uid: "1234567890"
}
```

- Überauth failed authentication response (Google OAuth2)

```
%Ueberauth.Failure{
errors: [
%Ueberauth.Failure.Error{
message: "Bad Request",
message_key: "invalid_grant"
}
],
provider: :google,
strategy: Ueberauth.Strategy.Google
}
```

## Troubleshooting

- **Phoenix is unable to create symlinks**

Known issue with Windows. It can be worked around but for now just ignore it.

- **error: Processing of js/socket.js failed. Error: Improperly-cased require: 'phoenix' in d:/code/Projects/absence_register/assets/js/socket.js**
- **Processing of js/app.js failed. Error: Improperly-cased require: 'phoenix_html' in d:/code/Projects/absence_register/assets/js/app.js**

Don't forget to `npm install` from `./assets/`. If you're on Windows you also need to ensure your command prompt / terminal is in the correctly-cased directory. So even though for example if your project is in `C:\Code\Learning\Elixir\absence_register`, you will encounter errors if you are in `C:\code\learning\elixir\absence_register`. Even though Windows does not treat paths as case-sensitive, Node.js still does at times.

- **(UndefinedFunctionError) function Ueberauth.init/1 is undefined (module Ueberauth is not available)**

Make sure you added ueberauth to your `mix.exs` correctly. Stop your server and run `mix deps.get` to install the newly added dependencies

- **function AbenceRegister.AuthController.request/2 is undefined or private**

Ensure that your `AuthController` calls `plug(Ueberauth)` and that you have specified the `/auth` routes in `router.ex`.

- **could not find a matching AbsenceRegisterWeb.AuthController.callback clause to process request**

There was a problem with your sign-in response. Look at the `Ueberauth.Failure` part of

- **assign @conn not available in eex template.**

Make sure you pass `conn` to the render template call, e.g. `<%= render "sometemplate.html", conn: @conn %>`

- **Postgrex.Protocol (#PID<x.x.x>) failed to connect: (Postgrex.Error) FATAL 3D000 (invalid_catalog_name): database "absence_dev" does not exist**

Run `ecto.create` before you run `ecto.migrate`.

- **Postgrex.Protocol (#PID<x.x.x>) failed to connect:(DBConnection.ConnectionError) tcp connect (localhost:5432): connection refused - :econnrefused**

Make sure your PostgreSQL server is running and you have put the correct connection credentials into `./config/config.exs` or `./config/dev.exs`.

- **undefined function absence_request_path/3**

Remember to add routes to `./lib/absence_register_web/router.ex` for any new controllers created by tools like `mix phx.gen.html`. The easiest (overkill) way is to add a line like `resources("/requests", AbsenceRequestController)`

- **The database for AbsenceRegister.Repo couldn't be created** (when running `mix test`)

Make sure you have the correct credentials in `./config/test.exs` - in this instance they will likely be identical to what you have in `./config/dev.exs` but with a different database named `absence_register_test` instead of `absence_register_dev`.

## Learn more

### Formal documentation

- Elixir official website: https://elixir-lang.org
- Elixir docs: https://elixir-lang.org/docs.html
- Phoenix official website: http://www.phoenixframework.org
- Phoenix guides: http://phoenixframework.org/docs/overview
- Phoenix docs: https://hexdocs.pm/phoenix
- Ecto docs: https://hexdocs.pm/ecto
- Absinthe docs: https://hexdocs.pm/absinthe
- Überauth Google website: https://github.com/ueberauth/ueberauth_google
- Guardian source: https://github.com/ueberauth/guardian
  _A fairly feature-heavy library for Überauth token based authentication_

### Exercises, examples

- Elixir examples: https://elixir-examples.github.io
- 30 Days of Elixir: https://github.com/seven1m/30-days-of-elixir
- exercism: https://exercism.io/tracks/elixir
- Hackerrank: https://hackerrank.com

### Tutorials, podcasts etc

- Alchemist Camp: https://alchemist.camp/episodes
  - [Screencast on setting up your editing environment](https://alchemist.camp/episodes/configuring-editor-plugins-and-formatter)
- ElixirCasts: https://elixircasts.io
- Daily Drip ($): https://www.dailydrip.com/topics/elixir
- LearnElixir.tv ($): https://www.learnelixir.tv
- LearnPhoenix.tv ($): https://www.learnphoenix.tv
- Elixir for Programmers ($): https://codestool.coding-gnome.com/courses/elixir-for-programmers
- Pluralsight "Meet Elixir" ($): https://www.pluralsight.com/courses/meet-elixir

### Conference videos

- Confreaks.tv: http://confreaks.tv/tags/40
  - [Elixir Conf EU](http://confreaks.tv/conferences/elixir-conf-eu)
  - [Elixir Conf](http://confreaks.tv/conferences/elixir-conf)
- ErlangSolutions Youtube channel: https://www.youtube.com/user/ErlangSolutions/
  - [ElixirConf EU 2018](https://www.youtube.com/watch?v=KGNfqiVF234&list=PLWbHc_FXPo2hJchaMDq_5FGn-lmGf-DAn)
  - [ElixirConf EU 2017](https://www.youtube.com/watch?v=kMHXd_iMGRU&list=PLWbHc_FXPo2jV6N5XEjbUQe2GkYcRkZdD)
  - [Elixir.LDN 2017](https://www.youtube.com/watch?v=zqzkrUVfv-k&list=PLWbHc_FXPo2h8_H-hZKYjNLTl3UraSyQ7)
  - [Lambda Days 2018](https://www.youtube.com/watch?v=RCU5WQDT8_8&list=PLWbHc_FXPo2jaxwnNB7KFEV7HYA0qHVxl)

### Jobs

- Plataformatec jobs board (official): http://plataformatec.com.br/elixir-radar/jobs
- Elixir Forum jobs board: https://elixirforum.com/c/elixir-jobs
- ElixirDose jobs board: https://elixir.career
- Elixir Jobs: https://elixirjobs.net/
- Stackoverflow jobs: https://stackoverflow.com/jobs/developer-jobs-using-elixir

### News

- ElixirStatus: http://elixirstatus.com/
- ElixirWeekly newsletter: https://elixirweekly.net/
- Elixir Radar (official): http://plataformatec.com.br/elixir-radar
- Elixir Digest: https://elixirdigest.net/

### Community

- Elixir Slack: https://elixir-slackin.herokuapp.com/
- Elixir Forum: https://elixirforum.com/

### Interesting companies

- Cultivate: https://cultivatehq.com/posts/
  _Major sponsor of many Elixir conferences and community meet-ups; occasional Elixir content on their blog_
- ## ThoughtBot: ThoughtBot: https://thoughtbot.com/services/elixir-phoenix
  _Creator of some of the more prominent Elixir libraries, as well as a blog and podcast featuring occasional Elixir content_

### Misc resources

- Elixir style guide: https://github.com/christopheradams/elixir_style_guide
- Elixir Fiddle: https://elixirfiddle.com/
- Elixir flash cards: https://elixircards.co.uk/
- awesome elixir: https://github.com/h4cc/awesome-elixir
- Elixir cheat sheet: http://media.pragprog.com/titles/elixir/ElixirCheat.pdf

```

```
