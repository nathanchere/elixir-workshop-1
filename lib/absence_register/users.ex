defmodule AbsenceRegister.Users do
  require Ecto.Query

  alias AbsenceRegister.Repo
  alias AbsenceRegister.User

  @spec list_users() :: any()
  def list_users do
    Repo.all(User)
  end

  @spec insert_or_update_user(%{email: String.t()}) :: any()
  def insert_or_update_user(%{email: email} = attrs) do
    changeset =
      case Repo.get_by(User, email: email) do
        nil ->
          User.changeset(%User{}, attrs)

        user ->
          User.changeset(user, Map.put(attrs, :id, user.id))
      end

    Repo.insert_or_update(changeset)
  end

  @spec get_user!(String.t()) :: User.t()
  def get_user!(email), do: Repo.get_by!(User, email: email)
end
