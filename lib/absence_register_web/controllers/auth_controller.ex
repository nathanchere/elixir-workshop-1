defmodule AbsenceRegisterWeb.AuthController do
  use AbsenceRegisterWeb, :controller
  plug(Ueberauth)

  alias AbsenceRegister.Users

  def callback(%{assigns: %{ueberauth_auth: auth}} = conn, _params) do
    user_attrs = %{
      token: auth.credentials.token,
      name: "#{auth.info.first_name} #{auth.info.last_name}",
      email: auth.info.email
    }

    signin(conn, user_attrs)
  end

  def callback(%{assigns: %{ueberauth_failure: auth}} = conn, _params) do
    [error | _] = auth.errors

    conn
    |> clear_session()
    |> put_flash(:error, error.message)
    |> redirect(to: page_path(conn, :index))
  end

  @spec signin(Plug.Conn.t(), %{email: binary()}) :: Plug.Conn.t()

  def signin(conn, user_attrs) do
    case Users.insert_or_update_user(user_attrs) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "Successfully signed in as #{user.name}")
        |> put_session(:user_id, user.email)
        |> redirect(to: page_path(conn, :index))

      {:error, _} ->
        conn
        |> clear_session()
        |> put_flash(:error, "Error signing in")
        |> redirect(to: page_path(conn, :index))
    end
  end

  def signout(conn, _params) do
    conn
    |> configure_session(drop: true)
    |> redirect(to: page_path(conn, :index))
  end
end
