defmodule AbsenceRegisterWeb.Plugs.SetUser do
  import Plug.Conn

  alias AbsenceRegister.Users

  def init(_params) do
  end

  def call(conn, _params) do
    if conn.assigns[:user] do
      conn
    else
      user_email = get_session(conn, :user_id)

      cond do
        user = user_email && Users.get_user!(user_email) ->
          assign(conn, :user, user)

        true ->
          assign(conn, :user, nil)
      end
    end
  end
end
